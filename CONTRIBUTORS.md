# Contributors

Many thanks to the following people for contributing to this project:

<!-- Add contributors alphabetically. -->

- Aravinth Manivannan, @realaravinth ([@realaravinth](https://gts.batsense.net/@realaravinth) on Fediverse)
- Arnold Schrijver, @circlebuilder ([@humanetech](https://social.coop/@smallcircles) on Fediverse)
