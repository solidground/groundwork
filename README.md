[![status-badge](https://ci.codeberg.org/api/badges/solidground/groundwork/status.svg)](https://ci.codeberg.org/solidground/groundwork)
---

<div align="center">

[![groundwork logo](groundwork-logo.png)](https://docs.solidground.work)

### [Project docs](https://docs.solidground.work/groundwork/project) | [Matrix space](https://matrix.to/#/#solidground-matters:matrix.org) | [Discussion forum](https://discuss.coding.social/c/solidground/13)

</div>

<br>

Welcome to Groundwork, the communication and integration platform for the Solidground project.

## Getting started

_(TODO)_

## Contributing

_(TODO)_

### Code of conduct

We practice [Social Coding Principles](https://coding.social/principles). By participating in our
projects you consent to our [Community Participation Guidelines](CODE_OF_CONDUCT.md).

### Credits

We are most thankful to our [Contributors](CONTRIBUTORS.md) who helped build this project. Please do not forget to add yourself to this list if you are one of them.

## License

Copyright (c) 2022 [Solidground](https://solidground.work) and contributors.

Floorplanner is licensed under [GNU Affero General Public License v3.0](LICENSE).